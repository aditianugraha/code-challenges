import React, { useEffect, useState } from 'react'
import '../styles/Challenge1.css'

export const Challenge1: React.FunctionComponent<{}> = () => {
  //local states
  const [loves, setLoves] = useState<Array<string>>([])
  const [delay, setDelay] = useState(false)
  const [count, setCount] = useState(0)

  //on click pinguin image
  const onPenguinClick = () => {
    //reset count
    setCount(0)

    //3 second condition
    if (!delay) setDelay(true)

    if (delay === false && loves.length < 5) {
      //add 1 item to array
      setLoves(prevState => [...prevState, 'loves'])
    } else {
      //reset array
      setLoves([])
    }
  }

  useEffect(() => {
    //30 second timer function
    const interval = setInterval(() => {
      setCount(oldCount => oldCount + 1)
    }, 1000)

    return () => {
      clearInterval(interval)
    }
  }, [])

  useEffect(() => {
    //3 second restricted delay
    setTimeout(() => {
      setDelay(false)
    }, 3000)
  }, [delay])

  useEffect(() => {
    //if 30 secound no click action then reset counter and delete 1 heart
    if (count >= 30) {
      const newList = loves.slice(1)
      setLoves(newList)
      setCount(0)
    }
  }, [count])

  return (
    <div>
      <div className="logoContainer">
        <img src={'/penguin.png'} className="logo" alt="logo" onClick={onPenguinClick} />
      </div>

      <div>
        <p>Give the penguin some love by tapping on it</p>
        Penguin loves meter: <br />
        {loves.map(() => (
          <img src="/love.png" />
        ))}
        {/* <img src="/love.png" /> */}
      </div>
    </div>
  )
}
